BUILD_TARGET=builder
DIST_PATH="./dist"
EXT ?= ""
GIT_COMMIT ?= $(shell git rev-parse HEAD)
PROJECT_HOST ?= "gitlab.com"
PROJECT_ORG ?= "mrsimonemms"
PROJECT_NAME ?= "terraform-plan-gui"
VERSION ?= $(shell cat VERSION)

LDFLAGS := "-s -w -X main.Version=$(VERSION) -X main.GitCommit=$(GIT_COMMIT)"

PROJECT_PATH = "/go/src/${PROJECT_HOST}/${PROJECT_ORG}/${PROJECT_NAME}"
TARGET = "./main.go"

build:
	mkdir -p ${DIST_PATH}
	CGO_ENABLED=0  go build \
		-ldflags $(LDFLAGS) \
		-o ${DIST_PATH}/${PROJECT_NAME}${EXT} ${TARGET}
.PHONY: build

build-all:
	make clean

	EXT=-darwin-amd64 GOARCH=amd64 GOOS=darwin make build

	EXT=-linux-arm GOARCH=arm GOOS=linux make build
	EXT=-linux-arm64 GOARCH=arm64 GOOS=linux make build
	EXT=-linux-amd64 GOARCH=amd64 GOOS=linux make build
	EXT=-linux-386 GOARCH=386 GOOS=linux make build

	EXT=-win-386.exe GOARCH=386 GOOS=windows make build
	EXT=-win-amd64.exe GOARCH=amd64 GOOS=windows make build
.PHONY: build-all

clean:
	rm -Rf ${DIST_PATH}
.PHONY: clean

debug:
	dlv debug --headless --listen=:2345 --api-version=2 --accept-multiclient
.PHONY: debug

docker-build:
	docker build \
	  	--target="${BUILD_TARGET}" \
		--build-arg=PROJECT_HOST=${PROJECT_HOST} \
		--build-arg=PROJECT_ORG=${PROJECT_ORG} \
		--build-arg=PROJECT_NAME=${PROJECT_NAME} \
		-t ${PROJECT_ORG}/${PROJECT_NAME} \
		.
.PHONY: docker-build

docker-run:
	docker run \
		-it \
		--rm \
		-v ${PWD}:${PROJECT_PATH} \
		-t ${PROJECT_ORG}/${PROJECT_NAME}
.PHONY: docker-run

install:
	go get -v -d ./...
.PHONY: install

run:
	@go run ${TARGET} ${ARGS}
.PHONY: run
